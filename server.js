const express = require('express')
const next = require('next')
const path = require('path')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const config = require('./database')
const User = require('./models/users.js')
const UserRoute = require('./routes/users.js')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
 
app.prepare()
   .then(() => {
     const server = express()
      
     // ###################### STATIC FILES #####################
     // #########################################################
     const staticDir = path.resolve(__dirname,'./','.next/static')
     server.use('/_next/static', express.static(staticDir))

     // ################## BODY-PARSER ######################### 
     // #########################################################
     server.use(bodyParser.json());
     server.use(bodyParser.urlencoded({ extended: false }));

     // ################# COOKIE PARSER #######################
     // #######################################################

     // ################# USE ROUTES #########################
     // ####################################################
     server.use('/user', UserRoute)
      
     // ################## DB CONNECTION #######################
     // #######################################################
     mongoose.connect(config.DB).then(
        () => { console.log('Database is connected') },
       err => { console.log('Cannot connect to the database' + err) }
     )

     // ################### PAGES ROUTING ######################
     // #########################################################
     server.get('/', (req, res) => {
       return app.render(req, res, 'index', req.query)
     })
     // #########################################################
 
     server.get('/register', (req, res) => {                                                                                                                                    
       return app.render(req, res, '/register', req.query)
     })
      
     // #########################################################

     server.get('/forgotPassword', (req, res) => {
       return app.render(req, res, '/forgotPassword', req.query)
     })

     // ########################################################

     server.get('/confirmRegistration', (req, res) => {
       return app.render(req, res, '/confirmRegistration', req.query)
     })

     // #######################################################

server.listen(port, (err) => {
  if (err) throw err
  console.log(`> Ready on http://localhost:${port}`)
})
})
