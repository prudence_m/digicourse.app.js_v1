var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
  user_name: {
    type: String,
  },
  user_email: {
    type: String,
  },
  user_password: {
    type: String,
  },
  user_pass_confirm: {
    type: String,
  }
});

var User = mongoose.model('User', UserSchema);
module.exports = User;
