const withCSS = require('@zeit/next-css')

module.exports = withCSS({
  webpack (config, options) {
    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use:[ 
      // this will create image copy, that we will use,
      // outputh - '/.next/static/longhash.png'
      // url - '/_next/static/longhash.png'
      {
        loader: 'url-loader',
        options: {
          limit: 100000,
          fallback: "file-loader",
          publicPath: "/_next/static/",
          outputPath: "static/",
        }
      }]
    })


    return config
  }
})
