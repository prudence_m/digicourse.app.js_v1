import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import dynamic from 'next/dynamic';

const Layout = dynamic(import('./lib/components/Layout/Layout') , { loading: () => null });
const Register = dynamic(import('./lib/components/Register/Register'), { loading: () => null });

const Register_user = () => (
  <Layout>
    <Register />
  </Layout>
)

export default Register_user;
