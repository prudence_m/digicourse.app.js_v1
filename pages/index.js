import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import dynamic from 'next/dynamic';


const Layout = dynamic(import('./lib/components/Layout/Layout'), { loading: () => null });
const Login = dynamic(import('./lib/components/Login/Login'), { loading: () => null });

const Index = () => (
  <Layout>
    <Login />
  </Layout>
)
export default Index;
