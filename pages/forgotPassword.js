import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import dynamic from 'next/dynamic';

const Layout = dynamic(import('./lib/components/Layout/Layout'), { loading: () => null });
const ForgotPassword = dynamic(import('./lib/components/ForgotPassword/ForgotPassword'), { loading: () => null });


const ForgotPassword_user = () => (                                                      
  <Layout>
    <ForgotPassword />
  </Layout>
)

export default ForgotPassword_user;

