import React from 'react';
import dynamic from 'next/dynamic';

const Layout = dynamic(import('./lib/components/Layout/Layout'), { loading: () => null });
const ConfirmRegistration = dynamic(import('./lib/components/confirmRegistration/confirmRegistration'), { loading: () => null });

const confirmRegistration_user = () => (
  <Layout>
    <ConfirmRegistration />
  </Layout>
)

export default confirmRegistration_user;
