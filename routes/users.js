const express = require('express');
const User = require('../models/users');
const app = express();
const router = express.Router();

router.route('/add').post(function(req, res) {
  const user = new User(req.body);
  user.save()
    .then(user => {
      res.status(200).json({'user': 'User added successfully'});
    })
    .catch(err => {
      res.status(400).send('unable to save the user into database' + err);
    });
});

module.exports = router;
